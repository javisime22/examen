package org.examen.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.examen.dto.EmpleadosDTO;
import org.examen.entities.Empleados;
import org.examen.entities.Generos;
import org.examen.entities.Trabajos;
import org.examen.repository.EmpleadosRepository;
import org.examen.repository.TrabajosRepository;
import org.examen.response.Response;
import org.examen.response.ResponseTrabajos;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

@ApplicationScoped
public class EmpleadosService {
	@Inject
	EmpleadosRepository empleadosRepository;
	
	@Inject
	TrabajosRepository trabajosRepository;
	
	public Response guardarEmpleado(EmpleadosDTO empleado) {
		Response res = new Response();
		Date hoy = new Date();
		
		Trabajos trabajo = trabajosRepository.findById(empleado.getJob_id());
		if(trabajo == null) {
			res.setId(null);
			res.setSuccess(false);
			return res;
			
		}
		Calendar c1 = Calendar.getInstance();
		Calendar c2 = Calendar.getInstance();
		c1.setTime(hoy);
		c2.setTime(empleado.getBirthdate());
		int yearDiff = c1.get(Calendar.YEAR) - c2.get(Calendar.YEAR);
		if(yearDiff <18) {
			res.setId(null);
			res.setSuccess(false);
			return res;
		}
		List<Object[]> valid = empleadosRepository.findNameAndLastname(empleado.getName(), empleado.getLast_name());
		if(!valid.isEmpty()) {
			res.setId(null);
			res.setSuccess(false);
			return res;
		}
		try {
			
		Empleados empleados = castEmpleado(empleado);
		empleados.setJob(trabajo);
		empleadosRepository.persist(empleados);
		res.setId(empleados.getId());
		res.setSuccess(true);
		}
		catch(Exception e) {
			
		}
		return res;
	}
	public Empleados castEmpleado(EmpleadosDTO empleado) {
		Empleados emp = new Empleados();
		emp.setJob(new Trabajos());
		emp.setGender(new Generos());
		emp.getGender().setId(empleado.getGender_id());
		emp.getGender().setName(empleado.getGender_id() == 1 ? "Hombre": "Mujer");
		emp.setBirthdate(empleado.getBirthdate());
		emp.setName(empleado.getName());
		emp.setLast_name(empleado.getLast_name());
		return emp;
	}
	public ResponseTrabajos obtenerEmpleados(List<Long> empleados) {
		ResponseTrabajos res = new ResponseTrabajos();
		List<Empleados> buscarEmpleados = empleadosRepository.findByIds(empleados);
		if(buscarEmpleados.isEmpty()) {
			res.setEmployees(null);
			res.setSuccess(false);
			return res;	
		}
		res.setEmployees(buscarEmpleados);
		res.setSuccess(true);
		return res;
	}
}
