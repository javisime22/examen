package org.examen.service;

import java.util.Comparator;
import java.util.List;

import org.examen.dto.TrabajosDTO;
import org.examen.entities.Empleados;
import org.examen.repository.EmpleadosRepository;
import org.examen.response.ResponseTrabajos;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

@ApplicationScoped
public class TrabajosService {
	@Inject
	EmpleadosRepository empleadosRepository;

	public ResponseTrabajos obtenerTrabajo(TrabajosDTO trabajo) {
		ResponseTrabajos res = new ResponseTrabajos();
		List<Empleados> empleados = empleadosRepository.findByJobId(trabajo.getJob_id());
		if(empleados.isEmpty()) {
			res.setSuccess(false);
			res.setEmployees(null);
			return res;
		}
		empleados.sort(Comparator.comparing(Empleados::getLast_name));
		res.setSuccess(true);
		res.setEmployees(empleados);
		return res;
	}
}
