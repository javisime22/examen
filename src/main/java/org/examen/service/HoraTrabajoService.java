package org.examen.service;

import java.util.List;

import org.examen.dto.HoraTrabajoDTO;
import org.examen.entities.HorasTrabajo;
import org.examen.repository.HoraTrabajoRepository;
import org.examen.response.ResponseHoraTrabajo;
import org.examen.response.ResponsePago;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

@ApplicationScoped
public class HoraTrabajoService {
	@Inject
	HoraTrabajoRepository horaTrabajoRepository;

	public ResponseHoraTrabajo obtenerHoras(HoraTrabajoDTO hora) {
		ResponseHoraTrabajo res = new ResponseHoraTrabajo();
		if (hora.getEnd_date().before(hora.getStart_date())) {
			res.setSucess(false);
			res.setTotal_worked_hours(null);
			return res;
		}
		List<HorasTrabajo> horaTrabajo = horaTrabajoRepository.findByIdAndDateRange(hora.getEmployee_id(),
				hora.getStart_date(), hora.getEnd_date());
		if (horaTrabajo.isEmpty()) {
			res.setSucess(false);
			res.setTotal_worked_hours(null);
			return res;
		}
		Integer sum = horaTrabajo.stream().mapToInt(o -> o.getWorkedhours().intValue()).sum();
		res.setSucess(true);
		res.setTotal_worked_hours(sum);
		return res;

	}

	public ResponsePago obtenerPago(HoraTrabajoDTO hora) {
		ResponsePago res = new ResponsePago();
		if (hora.getEnd_date().before(hora.getStart_date())) {
			res.setSuccess(null);
			res.setPayment(null);
			return res;
		}
		List<HorasTrabajo> horaTrabajo = horaTrabajoRepository.findByIdAndDateRange(hora.getEmployee_id(),
				hora.getStart_date(), hora.getEnd_date());
		if (horaTrabajo.isEmpty()) {
			res.setSuccess(null);
			res.setPayment(null);
			return res;
		}
		Integer sum = horaTrabajo.stream().mapToInt(o -> o.getEmployees().getJob().getSalary().intValue()).sum();
		res.setSuccess(true);
		res.setPayment(sum);
		return res;

	}
}
