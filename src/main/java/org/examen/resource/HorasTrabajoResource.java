package org.examen.resource;


import org.examen.dto.HoraTrabajoDTO;
import org.examen.response.ResponseHoraTrabajo;
import org.examen.response.ResponsePago;
import org.examen.service.HoraTrabajoService;

import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

@Path("/hora")
@Produces(MediaType.APPLICATION_JSON)
public class HorasTrabajoResource {
	@Inject
	HoraTrabajoService horaTrabajoService;
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Transactional
	public ResponseHoraTrabajo guardarEmpleado(HoraTrabajoDTO hora) {
		return horaTrabajoService.obtenerHoras(hora);
	}
	@POST
	@Path("/pago")
	@Consumes(MediaType.APPLICATION_JSON)
	@Transactional
	public ResponsePago horaPago(HoraTrabajoDTO pago) {
		return horaTrabajoService.obtenerPago(pago);
	}
}
