package org.examen.resource;

import org.examen.dto.TrabajosDTO;
import org.examen.response.ResponseTrabajos;
import org.examen.service.TrabajosService;

import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

@Path("/trabajos")
@Produces(MediaType.APPLICATION_JSON)
public class TrabajosResource {

	@Inject
	TrabajosService trabajoService;

	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Transactional
	public ResponseTrabajos obtenerTrabajos(TrabajosDTO trabajo) {
		return trabajoService.obtenerTrabajo(trabajo);
	}
}
