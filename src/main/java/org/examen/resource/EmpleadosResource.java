package org.examen.resource;

import java.util.List;

import org.examen.dto.EmpleadosDTO;
import org.examen.response.Response;
import org.examen.response.ResponseTrabajos;
import org.examen.service.EmpleadosService;

import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;

@Path("/empleados")
@Produces(MediaType.APPLICATION_JSON)
public class EmpleadosResource {
	@Inject
	EmpleadosService empleadosService;

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Transactional
	public Response guardarEmpleado(EmpleadosDTO empleados) {
		return empleadosService.guardarEmpleado(empleados);
	}

	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/multi")
	public ResponseTrabajos guardarEmpleado(@QueryParam("employee_id") List<Long> employee_id) {
		return empleadosService.obtenerEmpleados(employee_id);
	}
}
