package org.examen.dto;

import java.util.Date;

public class EmpleadosDTO {
	private String name;
	private String last_name;
	private Date birthdate;
	private Long job_id;
	private Long gender_id;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public Date getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	public Long getJob_id() {
		return job_id;
	}

	public void setJob_id(Long job_id) {
		this.job_id = job_id;
	}

	public Long getGender_id() {
		return gender_id;
	}

	public void setGender_id(Long gender_id) {
		this.gender_id = gender_id;
	}

	@Override
	public String toString() {
		return "EmpleadosDTO [name=" + name + ", last_name=" + last_name + ", birthdate=" + birthdate + ", job_id="
				+ job_id + ", gender_id=" + gender_id + "]";
	}

	public EmpleadosDTO() {
		super();
	}

}
