package org.examen.dto;

import java.util.Date;

public class HoraTrabajoDTO {
	public Long employee_id;
	public Date start_date;
	public Date end_date;
	public Long getEmployee_id() {
		return employee_id;
	}
	public void setEmployee_id(Long employee_id) {
		this.employee_id = employee_id;
	}
	public Date getStart_date() {
		return start_date;
	}
	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}
	public Date getEnd_date() {
		return end_date;
	}
	public void setEnd_date(Date end_date) {
		this.end_date = end_date;
	}
	public HoraTrabajoDTO() {
		super();
	}
	
	
	
	
}
