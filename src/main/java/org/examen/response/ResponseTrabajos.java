package org.examen.response;

import java.util.List;

import org.examen.entities.Empleados;

public class ResponseTrabajos {
	public List<Empleados> employees;
	public boolean success;
	
	
	public List<Empleados> getEmployees() {
		return employees;
	}


	public void setEmployees(List<Empleados> employees) {
		this.employees = employees;
	}


	public boolean isSuccess() {
		return success;
	}


	public void setSuccess(boolean success) {
		this.success = success;
	}


	public ResponseTrabajos() {
		super();
	}
	
	
}
