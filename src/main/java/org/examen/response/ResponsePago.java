package org.examen.response;

public class ResponsePago {
	public Integer payment;
	public Boolean success;
	public Integer getPayment() {
		return payment;
	}
	public void setPayment(Integer payment) {
		this.payment = payment;
	}
	public Boolean getSuccess() {
		return success;
	}
	public void setSuccess(Boolean success) {
		this.success = success;
	}
	public ResponsePago() {
		super();
	}

	
	
}
