package org.examen.response;

public class ResponseHoraTrabajo {
	public Integer total_worked_hours;
	public boolean sucess;

	public Integer getTotal_worked_hours() {
		return total_worked_hours;
	}

	public void setTotal_worked_hours(Integer total_worked_hours) {
		this.total_worked_hours = total_worked_hours;
	}

	public boolean isSucess() {
		return sucess;
	}

	public void setSucess(boolean sucess) {
		this.sucess = sucess;
	}

	public ResponseHoraTrabajo() {
		super();
	}
}
