package org.examen.repository;

import java.util.Date;
import java.util.List;

import org.examen.entities.HorasTrabajo;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class HoraTrabajoRepository implements PanacheRepository<HorasTrabajo>{
	 public List<HorasTrabajo> findByIdAndDateRange(Long id, Date startDate, Date endDate) {
			return find("employees.id = ?1 and worked_date between ?2 and ?3", id, startDate, endDate).list();
	    }
}
