package org.examen.repository;

import java.util.List;

import org.examen.entities.Empleados;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;

@ApplicationScoped
public class EmpleadosRepository implements PanacheRepository<Empleados> {
	@Inject
	EntityManager entityManager;

	@Transactional
	public List<Object[]> findNameAndLastname(String name, String lastname) {
		@SuppressWarnings("unchecked")
		List<Object[]> results = entityManager
				.createNativeQuery(
						"SELECT name, last_name FROM employees WHERE name = :name and last_name = :lastname ")
				.setParameter("name", name).setParameter("lastname", lastname).getResultList();
		return results;
	}

	public List<Empleados> findByJobId(Long jobId) {
		return list("job.id", jobId);
	}

	public List<Empleados> findByIds(List<Long> ids) {
		return list("id in ?1", ids);
	}
}
