package org.examen.repository;

import org.examen.entities.Trabajos;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class TrabajosRepository implements PanacheRepository<Trabajos> {

}
