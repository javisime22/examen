/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.examen.entities;

/**
 *
 * @author 980022884
 */

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;

import java.util.*;

@Entity
@Table(name = "JOBS")
public class Trabajos extends PanacheEntity {
	private Long id;
	private String name;
	private Double salary;

	public Trabajos() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getSalary() {
		return salary;
	}

	public void setSalary(Double salary) {
		this.salary = salary;
	}

	@Override
	public String toString() {
		return "Trabajos [id=" + id + ", name=" + name + ", salary=" + salary + "]";
	}

}
