/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.examen.entities;

/**
 *
 * @author 980022884
 */
import io.quarkus.hibernate.orm.panache.PanacheEntity;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

import java.util.Date;

@Entity
@Table(name = "EMPLOYEE_WORKED_HOURS")
public class HorasTrabajo extends PanacheEntity {

	private Long id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "employees_id", referencedColumnName = "id", nullable = false, insertable = false)
	private Empleados employees;

	private Double workedhours;

	private Date worked_date;

	public HorasTrabajo() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Empleados getEmployees() {
		return employees;
	}

	public void setEmployees(Empleados employees) {
		this.employees = employees;
	}

	public Double getWorkedhours() {
		return workedhours;
	}

	public void setWorkedhours(Double workedhours) {
		this.workedhours = workedhours;
	}

	public Date getWorked_date() {
		return worked_date;
	}

	public void setWorked_date(Date worked_date) {
		this.worked_date = worked_date;
	}

}
