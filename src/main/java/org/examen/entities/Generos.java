/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.examen.entities;

/**
 *
 * @author 980022884
 */

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import org.examen.entities.*;

@Entity
@Table(name = "GENDERS")
public class Generos extends PanacheEntity {
	private Long id;
	private String name;

	public Generos() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Generos [id=" + id + ", name=" + name + "]";
	}

}
