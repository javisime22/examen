/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.examen.entities;

/**
 *
 * @author 980022884
 */


import io.quarkus.hibernate.orm.panache.PanacheEntity;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

import org.examen.entities.*;
import java.util.Date;


@Entity
@Table(name = "EMPLOYEES")
public class Empleados extends PanacheEntity {
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name = "gender_id")
	    private Generos gender;
	    
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="job_id")

	    private Trabajos job;
	    
	    
	    private String name;
	    private String last_name;
	    private Date birthdate;

	    public Empleados() {
	    }

	    // Getters y setters

	    public String getName() {
	        return name;
	    }

		public Generos getGender() {
			return gender;
		}

		public void setGender(Generos gender) {
			this.gender = gender;
		}

		public Trabajos getJob() {
			return job;
		}

		public void setJob(Trabajos job) {
			this.job = job;
		}



		public String getLast_name() {
			return last_name;
		}

		public void setLast_name(String last_name) {
			this.last_name = last_name;
		}

		public Date getBirthdate() {
			return birthdate;
		}

		public void setBirthdate(Date birthdate) {
			this.birthdate = birthdate;
		}

		public void setName(String name) {
			this.name = name;
		}
		
		
		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		@Override
		public String toString() {
			return "Empleados [gender_id=" + gender + ", job_id=" + job + ", name=" + name
					+ ", last_name=" + last_name + ", birthdate=" + birthdate + "]";
		}

	   
		
	}